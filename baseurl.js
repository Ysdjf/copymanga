const baseURL = 'https://www.copymanga.org';
const Search_baseURL = 'https://www.copymanga.org/api/kb/web/searchs/comics?limit=20&offset={0}&platform=2&q={1}&q_type=';
const Chapter_baseURL = 'https://api.copymanga.org/api/v3/comic/{0}/group/default/chapters?limit=0';
const Page_baseURL = 'https://api.copymanga.org/api/v3/comic/{0}/chapter2/{1}';

module.exports = {
    baseURL: baseURL,
    Search_baseURL: Search_baseURL,
    Chapter_baseURL: Chapter_baseURL,
    Page_baseURL: Page_baseURL
};
