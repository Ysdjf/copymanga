const URL = require('./baseurl');
const baseURL = URL.baseURL;

class IndexController extends Controller {
    load() {
        this.data = {
            tabs: [
                {
                    "title": "推荐",
                    "id": "home",
                    "url": baseURL,
                },
                {
                    "title": "更新",
                    "id": "new",
                    "url": baseURL + "/comics?ordering=-datetime_updated"
                },
                {
                    "title": "热门",
                    "id": "hot",
                    "url": baseURL + "/comics?ordering=-popular"
                }, 
            ]
        };
    }
}

module.exports = IndexController;