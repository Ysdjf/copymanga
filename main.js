const URL = require('./baseurl');
const baseURL = URL.baseURL;

class MainController extends Controller {

    load(data) {
        this.id = data.id;
        this.url = data.url;
        this.page = 0;

        var cached = this.readCache();
        let list;
        if (cached) {
            list = cached.items;
        } else {
            list = [];
        }

        this.data = {
            list: list,
            loading: false,
            hasMore: this.id != 'home'
        };

        this.userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:101.0) Gecko/20100101 Firefox/101.0';

        if (cached) {
            let now = new Date().getTime();
            if (now - cached.time > 30 * 60 * 1000) {
                this.reload();
            }
        } else {
            this.reload();
        }

    }

    async onPressed(index) {
        await this.navigateTo('book', {
            data: this.data.list[index]
        });
    }

    onRefresh() {
        this.reload();
    }

    async onLoadMore() {
        this.setState(() => {
            this.data.loading = true;
        });
        try {
            let page = this.page + 1;
            let url = this.makeURL(page);
            let res = await fetch(url, {
                headers: {
                    'User-Agent': this.userAgent,
                },
            });
            let text = await res.text();
            this.page = page;
            let items = this.parseData(text, url);
    
            this.setState(()=>{
                for (let item of items) {
                    this.data.list.push(item);
                }
                this.data.loading = false;
                this.data.hasMore = this.id != 'home' && items.length != 0;
                console.log(`id: ${this.id} ${this.data.hasMore}`)
            });
        } catch (e) {
            showToast(`${e}\n${e.stack}`);
            this.data.hasMore = false;
            this.setState(()=>{
                this.data.loading = false;
            });
        }
        
    }

    makeURL(page) {
        if (this.id == 'home') {
            return this.url;
        } else {
            return this.url + `&offset=${page * 50}&limit=50`;
        }
    }

    async reload() {
        this.setState(() => {
            this.data.loading = true;
        });
        try {
            let url = this.makeURL(0);
            let res = await fetch(url, {
                headers: {
                    'User-Agent': this.userAgent,
                },
            });
            let text = await res.text();
            let items = this.parseData(text, url);
            this.page = 0;
            localStorage['cache_' + this.id] = JSON.stringify({
                time: new Date().getTime(),
                items: items,
            });
            this.setState(()=>{
                this.data.list = items;
                this.data.loading = false;
                this.data.hasMore = this.id != 'home' && items.length != 0;
            });
        } catch (e) {
            showToast(`${e}\n${e.stack}`);
            this.setState(()=>{
                this.data.loading = false;
            });
        }
    }

    readCache() {
        let cache = localStorage['cache_' + this.id];
        if (cache) {
            let json = JSON.parse(cache);
            return json;
        }
    }

    parseData(text, url) {
        if (this.id == 'home') {
            return this.parseHomeData(text);
        } else {
            return this.parsePageData(text);
        } 
    }

    parseHomeData(html) {
        const doc = HTMLParser.parse(html);
        let results = [];
        let items = doc.querySelectorAll('.col-auto');
        for (let item of items) {
            results.push({
                title: item.querySelector('a').textContent.replaceAll('\n','').replaceAll('  ',''),
                subtitle: '',
                link: item.querySelector('a').getAttribute('href').replace('/comic/', ''),
                picture: item.querySelector('img').getAttribute('data-src'),
                pictureHeaders: {
                    Referer: baseURL
                },
            });
        }
        return results;
    }

    parsePageData(text) {
        const doc = HTMLParser.parse(text);
        let results = [];
        let data_str = doc.querySelector('.exemptComic-box').getAttribute('list');
        let datas = data_str.substring(2, data_str.length - 2).split(']}, {');
        for (let data of datas) {
            let item = {};
            item.link = data.substring(data.search("path_word': '") + 13, data.search("', 'name':"));
            item.picture = data.substring(data.search("'cover': '") + 10, data.search("', 'status':"));
            item.title = data.substring(data.search("'name': '") + 9, data.search("', 'cover':"));
            item.subtitle = '';
            item.pictureHeaders = {
                Referer: baseURL
            };
            results.push(item);
        }
        return results;
    }
}

module.exports = MainController;
