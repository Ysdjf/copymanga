const bookFetch = require('./book_fetch');
const URL = require('./baseurl');
const Page_baseURL = URL.Page_baseURL;
const baseURL = URL.baseURL;

/**
 * @property {String}key need override the key for caching
 * @method load need override,
 * @method checkNew need override
 */
class MangaProcesser extends Processor {

    // The key for caching data
    get key() {
        return this.data.link;
    }

    /**
     * 
     * Start load pictures
     * 
     * @param {*} state The saved state.
     */
     async load(state) {
        try {
            let url_info = this.data.link.split(',');
            let url = Page_baseURL.replace('{1}', url_info[0]).replace('{0}', url_info[1]);
            let res = await fetch(url, {
                headers: {
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:101.0) Gecko/20100101 Firefox/101.0',
                }
            });
            let text = await res.text();
            let js = JSON.parse(text);
            let data = [];
            for (let img_url of js['results']['chapter']['contents']) {
                data.push({
                    url: img_url['url'],
                    headers: {
                        Referer: baseURL
                    }
                });
            }
            this.setData(data);
            this.save(true, state);
            this.loading = false;
        } catch (e) {
            showToast(`err ${e}\n${e.stack}`);
            this.loading = false;
        }
    }

    // Called in `dispose`
    unload() {

    }

    // Check for new chapter
    async checkNew() {
        let url = this.data.link;
        let data = await bookFetch(url);
        var item = data.list[data.list.length - 1];
        /**
         * @property {String}title The last chapter title.
         * @property {String}key The unique identifier of last chpater.
         */
        return {
            title: item.title,
            key: item.link,
        };
    }
}

module.exports = MangaProcesser;
