const URL = require('./baseurl');
const baseURL = URL.baseURL;
const Chapter_baseURL = URL.Chapter_baseURL;

function parseData(text, info) {
    const js = JSON.parse(text);
    let results = [];
    for (let list of js['results']['list']) {
        results.push({
            link: list['uuid'] + ',' + list['comic_path_word'],
            title: list['name'],
        });
    }
    return {
        subtitle: info['author_name'],
        summary: info['summary'],
        state : info['state'],
        list: results,
    };
}

module.exports = async function(url) {
    let manga_url = Chapter_baseURL.replace('{0}', url);
    try {
    let res = await fetch(baseURL + '/comic/' + url, {
        headers: {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:101.0) Gecko/20100101 Firefox/101.0',
        }
    });
    let text = await res.text();
    const doc = HTMLParser.parse(text);
    let info = doc.querySelector('.comicParticulars-title-right').querySelectorAll('li');
    var author_name = '';
    for (let author of info[2].querySelectorAll('a')) {
        author_name = author_name + author.textContent;
    }
    var state = info[5].querySelectorAll('span')[1].textContent;
    var summary = doc.querySelector('.intro').textContent.replaceAll('\n','').replaceAll('  ','');
    } catch (e) {
        var author_name = '';
        var summary = '';
        var state ='';
    }
    let res = await fetch(manga_url, {
        headers: {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:101.0) Gecko/20100101 Firefox/101.0',
        }
    });
    let text = await res.text();
    return parseData(text, {
        subtitle: author_name,
        summary: summary,
        state : state
    });
}